import numpy as np
import pylab as pl
import urllib2
import io

"""
opener = urllib.FancyURLopener({})
f=opener.open("http://www.python.org/")
Lines=f.read()
"""

#apparently the way below is better for grabbing data from url
#note it reads the html file but I just want the text ... hmm for future methinks!

data=urllib2.urlopen("http://www.python.org/").read(20000) #read only 20,000 chars
data = data.split("\n")

#file='Andrew_Peter_McMahon_CV.tex' #'README'

#store words in dictionary
dict = {}
for lines in data:
	words=lines.split()
	for word in words:
		if word in dict:
			dict[word] += 1
		else:
			dict[word] = 1

"""
#loop over lines
with open(file) as fileObject:
	for lines in fileObject:
		words=lines.split()
		for word in words:
			if word in dict:
				dict[word] += 1
			else:
				dict[word] = 1
"""
#create new dict for interesting cases
dictInteresting = {}
			
#freq tolerance for interest
freqTol=10

for key in dict:
	if int(dict[key]) > freqTol:
		dictInteresting[key] = dict[key]
print dictInteresting

#plot 'interesting' results
Xbins=np.arange(len(dictInteresting))
pl.bar(Xbins,dictInteresting.values(),width=0.5,color='g')
pl.xticks(Xbins,dictInteresting.keys())
pl.show()	
